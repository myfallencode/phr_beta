import Vue from 'vue'
import Vuex from 'vuex'
//import bcData from './fake/bcData';
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        callbacks: {},
        global: {},
        results: [1, 2, 3, 4, 5],
        user: {
            auth: false,
            login: '',
            id: 0,
            group: 0,
            profile: {}
        },
        count: 555,
        blockChoosen: {
            loaded: false,
            data: {}
        },
        isBcDataLoaded: false,
        bcData: {info: {Renters: {}}, buildFormatParams: {techparams: []}}
    },
    getters: {
        getterAuth: state => state.user,
        getProfile: state => state.user.profile,
        profileFullName: state => state.user.profile.Surname + ' ' + state.user.profile.Name + ' ' + state.user.profile.Middlename,
        getterResults: state => {
            return state.results.filter(function (n) {
                return n % 2
            })
        },
        getterResultsAll: state => {
            return state.results;
        },
        bcData: state => state.bcData,
        bcDataRenters: state => state.bcData.info.Renters,
        bcDataImages: state => state.bcData.info.Images,
        bcDataBlocks: state => state.bcData.info.Blocks,
        bcDataLoaded: state => state.isBcDataLoaded,
        bcDataBlockChoosen: state => state.blockChoosen,
        global: state => {
            return state.global;
        },
    },
    mutations: {
        setCallback(state, name) {
            state.callbacks[name[0]] = name[1];
        },
        executeCallback(state, name) {
            if (state.callbacks[name]) {
                return state.callbacks[name]();
            }
            else
                return false;

        },
        setGlobal(state, global) {
            state.global = global;
            window.global = global;
        },
        setAuth(state, item) {
            //console.log('Item => ', item);
            state.user.auth = item.token;
            state.user.login = item.login;
            state.user.id = item.id;
            state.user.profile = item.profile;
            state.user.group = item.group;
            //console.log('State =>', state);
        },
        setResults(state, item) {
            state.results.push(item);
        },
        setBcDataLoaded(state, bool) {
            state.isBcDataLoaded = bool;
        },
        setBcData(state, item) {
            state.bcData = item;
        },

        setProfile(state, profile) {
            state.user.profile = profile;
        },
        apiLoadProfile(state, callback) {
            apiAjax('profile/get', {}, function (response) {

                if (response.profile) {
                    store.commit('setProfile', response.profile);
                }

                if (callback) {
                    callback(response);
                }
            }, {async: false});
        },
        asetBlockChoosen(state, item){

            state.blockChoosen = item;
        }
    },
    actions: {
        loadProfile: function ({commit}, callback) {
            store.commit('apiLoadProfile', callback);
        },
        getCallback: function ({commit}, name) {
            store.commit('executeCallback', name);

        },
        actionAuth({commit}, query) {
            store.commit('setAuth', query);
        },
        actionResults({commit}, query) {
            store.commit('setResults', query);
        },
        bcDataLoaded({commit}, query) {
            store.commit('setBcDataLoaded', query);
        },
        bcData({commit}, query) {
            //console.log('BC_ACTION=>', query);
            store.commit('setBcData', query);
        },
        setBlockChoosen({commit}, query) {
            store.commit('asetBlockChoosen', query);
        }
    }
});

export default store