let bcData = {
    name: 'Бизнес-центр «1 Zhukov»',
    address: 'Проспект Маршала Жукова, 1 с1',
    district: 'СЗАО',
    metro: {
        name: 'Полежаевская',
        time: '9 минут транспортом'
    },
    rating: 8.5,
    geo: {
        x: 55555,
        y: 66666
    },
    renters: [
        {
            name: 'Some company',
            description: 'lorem ipsum 11111',
            logo: 'https://ph.ru/images/companies/57534_0/w200/h100/nocrop/57534.jpg'
        },
        {
            name: 'Some company 2',
            description: 'lorem ipsum 22222',
            logo: ''
        },
        {
            name: 'Some company 3',
            description: 'lorem ipsum 33333',
            logo: ''
        }
    ],
    params: {
        main: [
            {
                key: 'Обшая площадь',
                value: '22 550 м2'
            },
            {
                key: 'Год реконструкции',
                value: '22 550 м2'
            },
            {
                key: 'Этажность',
                value: '50'
            },
            {
                key: 'Лифты',
                value: '4'
            },
            {
                key: 'Паркинг',
                value: 'Охраняемый наземный паркинг'
            }
        ],
        engine: [
            {
                key: 'Кондиционирование',
                value: 'Центральное'
            },
            {
                key: 'Система оповещения о пожаре',
                value: 'Есть'
            }
        ],
        arch: [
            {
                key: 'Кондиционирование111',
                value: 'Центральное111'
            },
            {
                key: 'Система оповещения о пожаре',
                value: 'Есть'
            }
        ],
        tele: [
            {
                key: 'Кондиционирование222',
                value: 'Центральное222'
            },
            {
                key: 'Система оповещения о пожаре',
                value: 'Есть'
            }
        ],
        security: [
            {
                key: 'Кондиционирование333',
                value: 'Центральное333'
            },
            {
                key: 'Система оповещения о пожаре',
                value: 'Есть'
            }
        ],
        parking: [
            {
                key: 'Кондиционирование444',
                value: 'Центральное444'
            },
            {
                key: 'Система оповещения о пожаре',
                value: 'Есть'
            }
        ],
    }
}; // Object end

export default bcData;