import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index.js'
//Common
import Header from '../HeaderView.vue'
import HeaderAuth from '../HeaderAuthView.vue'
import Footer from '../FooterView.vue'
import Support from '../support/SupportView.vue';
//Auth
import Login from '../auth/LoginView.vue'
import RestorePwd from '../auth/RestorePasswordView.vue'
import Register from '../auth/RegisterView.vue'
//Search
import Search from '../search/searchView.vue'
import SearchSelectedItems from '../search/searchSelectedItems.vue'
//Clients
import ClientList from '../client/ClientListView.vue'
import ClientAdd from '../client/ClientAddView.vue'
//Catalog
import CatalogObject from '../catalog/CatalogObjectView.vue'
import CatalogObjectDetails from '../catalog/CatalogObjectDetailsView.vue'
import codMain from '../catalog/CatalogObjectDetailsMainView.vue'
import codOffices from '../catalog/CatalogObjectDetailsOfficesView.vue'
import codRenters from '../catalog/CatalogObjectDetailsRentersView.vue'
import codLocation from '../catalog/CatalogObjectDetailsLocationView.vue'
import codMore from '../catalog/CatalogObjectDetailsMoreView.vue'
//About
import About from '../about/AboutView.vue'
import index from '../indexView.vue';
import AboutMain from '../about/AboutMainView.vue'
import AboutAgent from '../about/AboutAgentView.vue'
import AboutOwner from '../about/AboutOwnerView.vue'
import Privacy from '../PrivacyView.vue'
import Agreement from '../AgreementView.vue'
import PersonalData from '../PersonalDataView.vue'
import ProfileEdit from '../profile/ProfileEdit.vue'
//Profile
//test
import Playground from '../modal/playgroundView.vue'
import TestVuex from '../test/testView.vue'
import Tabs from '../common/tabs.vue'

Vue.use(Router);

let router = new Router({
    mode: 'history', //HTML5 History API mode
    routes: [
        {
            path: '/',
            name: 'index',
            components: {
                main: index
            }
        },
        //test
        {
            path: '/modal',
            components: {
                main: Playground,
                header: Header,
                footer: Footer
            },
            name: 'playground'
        },
        {
            path: '/test',
            components: {
                main: TestVuex,
                header: Header,
                footer: Footer
            },
            name: 'test'
        },

        {
            path: '/catalog/:symbol?',
            components: {
                main: CatalogObject,
                footer: Footer,
                header: Header,
            },
            meta: {requiresAuth: true},
            props: true,
            name: 'catalog_search'
        },
        {
            path: '/bc/:address',
            components: {
                main: CatalogObjectDetails,
                footer: Footer,
                header: Header,
            },
            name: 'bc_test',
            props: true,
            meta: {requiresAuth: true},
            children: [
                {
                    path: '/',
                    components: {
                        bc_tab: codMain,
                        footer: Footer,
                        header: Header,
                    },
                    name: 'bc_main'
                },
                {
                    path: 'offices',
                    components: {
                        bc_tab: codOffices,
                        footer: Footer,
                        header: Header,
                    },
                    name: 'bc_offices'
                },
                {
                    path: 'offices/:blockid',
                    components: {
                        bc_tab: codMore,
                        footer: Footer,
                        header: Header,
                    },
                    name: 'bc_more'
                },
                {
                    path: 'renters',
                    components: {
                        bc_tab: codRenters,
                        footer: Footer,
                        header: Header,
                    },
                    name: 'bc_renters'
                },
                {
                    path: 'location',
                    components: {
                        bc_tab: codLocation,
                        footer: Footer,
                        header: Header,
                    },
                    name: 'bc_location'
                }

            ]
        },
        {
            path: '/search/:pageName([A-Za-z]+)?/:ClientID([1-9]\\d*)?',
            name: 'search',
            components: {
                main: Search,
                footer: Footer,
                header: Header,
            },
            //meta: {requiresAuth: true},
        },
        {
            path: '/login',
            components: {
                main: Login,
                header: HeaderAuth,
            },
            name: 'auth_login'

        },
        {
            path: '/restorePassword',
            components: {
                main: RestorePwd,
                header: HeaderAuth,
            },
            name: 'auth_restore_pwd'

        },
        {
            path: '/register',
            components: {
                main: Register,
                header: HeaderAuth,
            },
            name: 'auth_register'

        },
        {
            path: '/profile/edit',
            components: {
                main: ProfileEdit,
                footer: Footer,
                header: Header,
            },
            meta: {requiresAuth: true}
        },
        {
            path: '/clients',
            name: 'clients',
            components: {
                main: ClientList,
                footer: Footer,
                header: Header,
            },
            meta: {requiresAuth: true}
        },
        {
            path: '/clients/:act(add|edit)/:ClientID([1-9]\\d*)?',
            components: {
                main: ClientAdd,
                header: Header,

            },
            name: 'client_add',
            meta: {requiresAuth: true}
        },
        {
            path: '/about',
            components: {
                main: About,
                header: Header,
            },
            children: [
                {
                    path: '',
                    components: {
                        aboutmain: AboutMain,
                        header: Header,
                    },
                    name: 'about_main_content'
                },
                {
                    path: 'agent',
                    components: {
                        aboutmain: AboutAgent,
                        header: Header,
                    },
                    name: 'about_agent_content'
                },
                {
                    path: 'owner',
                    components: {
                        aboutmain: AboutOwner,
                        header: Header,
                    },
                    name: 'about_owner_content'
                }
            ]
        },
        {
            path: '/privacy',
            components: {
                main: Privacy,
                footer: Footer,
                header: Header,
            },
            name: 'privacy'
        },
        {
            path: '/agreement',
            components: {
                main: Agreement,
                footer: Footer,
                header: Header,
            },
            name: 'agreement'
        },
        {
            path: '/personal-data',
            components: {
                main: PersonalData,
                footer: Footer,
                header: Header,
            },
            name: 'personal_data'
        },
        {
            path: '/support',
            components: {
                main: Support,
                footer: Footer,
                header: Header,
            },
            name: 'support'
        },
        {
            path: '/tabs',
            components: {
                main: Tabs,
                footer: Footer,
                header: Header,
            },
            name: 'tabs'
        }
    ]
});
router.beforeEach((to, from, next) => {
    let self = this;
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // этот путь требует авторизации, проверяем залогинен ли
        // пользователь, и если нет, перенаправляем на страницу логина
        //console.log('ROUTER => ', store.getters.getterAuth);
        let user = store.getters.getterAuth;
        //console.log('USER', user);
        if (user.auth !== true) {
            next({
                path: '/login'
            })
        } else {
            next()
        }
    } else {
        next() // всегда так или иначе нужно вызвать next()!
    }
});

export default router;
