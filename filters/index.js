const urlParser = document.createElement('a');
import _ from 'lodash';

export function domain(url) {
    urlParser.href = url;
    return urlParser.hostname;
}

export function count(arr) {
    return arr.length;
}

export function prettyDate(date) {
    let a = new Date(date);
    return a.toDateString();
}

export function prettyShortDate(value) { // 21 сентября
    if(!value) value = '';
    let date = value.split('-');
    let month = '';
    switch (parseInt(date[1])) {
        case 0:
            month = 'января';
            break;
        case 1:
            month = 'февраля';
            break;
        case 2:
            month = 'марта';
            break;
        case 3:
            month = 'апреля';
            break;
        case 4:
            month = 'мая';
            break;
        case 5:
            month = 'июня';
            break;
        case 6:
            month = 'июля';
            break;
        case 7:
            month = 'августа';
            break;
        case 8:
            month = 'сентября';
            break;
        case 9:
            month = 'октября';
            break;
        case 10:
            month = 'ноября';
            break;
        case 11:
            month = 'дукабря';
            break;
    }
    return date[2] + ' ' + month;
}

export function prettyAddress(address) {
    return _.upperFirst(_.trim(_.replace(address, /^Моск.*?,/, '')));
}

export function prettyDigits(value) {
    return value.toString().replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1");
}

export function seconds(seconds) {
    seconds = parseInt(seconds);
    if (seconds > 0) {
        return seconds + " с.";
    }
    return "";
}

export function dataFromDb(str) {
    return str.split('-').reverse().join('.');
}