import Vue from 'vue';
import App from './App.vue';
import router from './router';
//import vmodal from 'vue-js-modal';
import {count, dataFromDb, domain, prettyDate, prettyShortDate, prettyAddress, prettyDigits, seconds} from './filters'
import {VueMaskDirective} from 'v-mask'
//import {VTooltip} from 'v-tooltip'
import $ from 'jquery';
import store from './store'
import * as pagination from "vuejs-uib-pagination";


window.SITE_URL = process.env.SITE_URL;
window.global = {};

window.setMeta = function(meta)
{
    if(meta)
    {
        if(meta.title)
        {
            $('title').html(meta.title);
        }
        if(meta.description)
        {
            $("meta[name='description']").attr('content', meta.description);
        }
        if(meta.keywords)
        {
            $("meta[name='keywords']").attr('content', meta.keywords);
        }
    }
}

window.apiAjax = function (urlPart, data, callback, params) {
    if (!params) params = {};
    if (typeof params.async == 'undefined') params.async = true;

    data._location = document.location.href;

    var ajaxParams = {
        'url': SITE_URL + 'api/' + urlPart + '/',
        'data': data,
        async: params.async,
        method: 'POST',
        success: function (responseData) {

            setMeta(responseData.meta);

            if (responseData.redirectURL) {
                document.location.href = responseData.redirectURL;
                return false;
            }

            if (callback) {
                callback(responseData);
            }
        },
        xhrFields: {
            withCredentials: true
        },
        error: function (error) {

        }
    };

    var executeFunction = function(token)
    {

        if(token)
        {
            ajaxParams.data.recaptchaToken = token;
        }

        $.ajax(ajaxParams);
    }

    if(params.recaptcha)
    {
        return useRecaptcha({ 'callback': function(token){
            executeFunction(token);
        }});
    }
    else
    {
        executeFunction();
    }

};

apiAjax('auth/init', {}, function (data) {
    console.log('APP user check data => ', data);

    //Глобальные переменные
    if (data.global)
        store.commit('setGlobal', data.global);

    if (data.auth === true) {
        console.log('Auth TRUE', data);
        store.dispatch('actionAuth', {
            token: data.auth,
            login: data.user.login,
            group: data.user.group,
            id: data.user.user,
            profile: data.profile
        });

        if(document.location.pathname == '/')
        {
            router.push('/clients')
        }
    } else {
        store.dispatch('actionAuth', {token: data.auth, login: '', id: 0, profile: {}});
    }
}, {async: false});

//Filters
Vue.filter('count', count);
Vue.filter('domain', domain);
Vue.filter('prettyDate', prettyDate);
Vue.filter('prettyDigits', prettyDigits);
Vue.filter('prettyShortDate', prettyShortDate);
Vue.filter('seconds', seconds);
Vue.filter('dataFromDb', dataFromDb);
Vue.filter('prettyAddress', prettyAddress);

Vue.directive('mask', VueMaskDirective);
//Vue.directive('tooltip', VTooltip);

Vue.use({router: router});
Vue.use(pagination);
/*new Vue({
    router,
    data:{
        isAuth: false
    }
}).$mount('#app'); //*/

window.VueApp =
    new Vue({
        el: '#app',
        router,

        computed: {
            global(){
                return this.$store.getters.global;
            }
        },
        methods:{
            clone(data){
                return JSON.parse(JSON.stringify(data));
            },
            login(){

            },
            getUser()
            {
                return store.getters.getterAuth;
            },
            logout(){
                let self = this;
                apiAjax('auth/logout', {}, function(response){
                    if(response.success)
                    {
                        self.$router.push('/');
                    }
                });
            },
        },
        store,
        template: '<App/>',
        mounted(){
            $(this.$el).addClass('page-wrapper')
        },
        components: {App}
    });
